import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";

import { Strategy, ExtractJwt} from "passport-jwt";
import { UsersRepository } from "./users.repository";
import { User } from "./user.entity";
import { JwtPayload } from "./jwt-payload.interface";

 
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        @InjectRepository(UsersRepository)
        private userRepository : UsersRepository,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey : 'secret'
        });
    }
 
    async validate(payload: JwtPayload): Promise<User> {
        console.log("payload",payload);
        const { mail } = payload;
        const user = await this.userRepository.findOne({ mail });
        if(!user){
            throw new UnauthorizedException();
        }
        return user;
    }
}